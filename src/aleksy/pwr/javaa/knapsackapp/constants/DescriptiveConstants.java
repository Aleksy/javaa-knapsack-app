package aleksy.pwr.javaa.knapsackapp.constants;

/**
 * Descriptive constants of Knapsack Application
 */
public class DescriptiveConstants {
   /**
    * Application name
    */
   public static final String APP_NAME = "Knapsack Application";
   /**
    * Application version
    */
   public static final String VERSION = "0.1.0";
   /**
    * Author
    */
   public static final String AUTHOR = "Aleksy Bernat - Politechnika Wrocławska";
}

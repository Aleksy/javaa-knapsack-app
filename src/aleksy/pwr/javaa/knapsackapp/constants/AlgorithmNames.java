package aleksy.pwr.javaa.knapsackapp.constants;

import aleksy.pwr.javaa.knapsacklib.common.impl.KnapsackLibrary;

/**
 * Algorithm names
 */
public class AlgorithmNames {
   /**
    * Greedy algorithm
    */
   public static final String GREEDY = KnapsackLibrary.newInstance().getGreedyDescription().getTitle();
   /**
    * Random search algorithm
    */
   public static final String RANDOM_SEARCH = KnapsackLibrary.newInstance().getRandomSearchDescription().getTitle();
   /**
    * Brute force algorithm
    */
   public static final String BRUTE_FORCE = KnapsackLibrary.newInstance().getBruteForceDescription().getTitle();

}

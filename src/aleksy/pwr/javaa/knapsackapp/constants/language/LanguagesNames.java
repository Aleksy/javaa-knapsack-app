package aleksy.pwr.javaa.knapsackapp.constants.language;

/**
 * Available language names
 */
public class LanguagesNames {
   /**
    * English language
    */
   public static final String ENGLISH = "EN";
   /**
    * Polish language
    */
   public static final String POLISH = "PL";
}

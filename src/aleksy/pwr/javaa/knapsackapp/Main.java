package aleksy.pwr.javaa.knapsackapp;

import aleksy.pwr.javaa.knapsackapp.constants.DescriptiveConstants;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

   @Override
   public void start(Stage primaryStage) throws Exception {
      Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
      primaryStage.setTitle(DescriptiveConstants.APP_NAME + " " + DescriptiveConstants.VERSION);
      primaryStage.setScene(new Scene(root, 780, 455));
      primaryStage.show();
   }

   public static void main(String[] args) {
      launch(args);
   }
}

package aleksy.pwr.javaa.knapsackapp;

import aleksy.pwr.javaa.knapsackapp.constants.AlgorithmNames;
import aleksy.pwr.javaa.knapsackapp.constants.language.LanguagesNames;
import aleksy.pwr.javaa.knapsacklib.common.api.KnapsackLibraryApi;
import aleksy.pwr.javaa.knapsacklib.common.impl.KnapsackLibrary;
import aleksy.pwr.javaa.knapsacklib.common.model.Description;
import aleksy.pwr.javaa.knapsacklib.common.model.Item;
import aleksy.pwr.javaa.knapsacklib.common.model.Knapsack;
import aleksy.pwr.javaa.knapsacklib.configuration.impl.RandomSearchConfig;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;
import java.util.ResourceBundle;

public class Controller implements Initializable {
   @FXML
   private Button startButton;

   @FXML
   private Button addButton;

   @FXML
   private Button createRandomButton;

   @FXML
   private TextField weightOfItemTextField;

   @FXML
   private TextField worthOfItemTextField;

   @FXML
   private Label weightOfItemLabel;

   @FXML
   private Label worthOfItemLabel;

   @FXML
   private ListView<Item> itemListView;

   @FXML
   private ListView<Item> resultListView;

   @FXML
   private ComboBox<String> selectAlgorithm;

   @FXML
   private TextField capacity;

   @FXML
   private Label algorithmNotChoosenLabel;

   @FXML
   private TextArea algorithmDescriptionTextArea;

   @FXML
   private ImageView knapsackImage;

   @FXML
   private Label itemPropertiesLabel;

   @FXML
   private Label weightLabel;

   @FXML
   private Label worthLabel;

   @FXML
   private Label numberOfItemsLabel;

   @FXML
   private Label resultLabel;

   @FXML
   private Label weightOfItems;

   @FXML
   private Label worthOfItems;

   @FXML
   private Label knapsackCapacityLabel;

   @FXML
   private Menu fileMenu;

   @FXML
   private MenuItem closeMenuItem;

   @FXML
   private Menu editMenu;

   @FXML
   private Menu languageMenu;

   @FXML
   private MenuItem polishMenuItem;

   @FXML
   private MenuItem englishMenuItem;

   @FXML
   private Menu helpMenu;

   @FXML
   private MenuItem aboutMenuItem;

   @FXML
   private void onStart() {
      KnapsackLibraryApi knapsackLib = KnapsackLibrary.newInstance();
      Knapsack knapsack = null;
      if (selectAlgorithm.getSelectionModel().getSelectedItem() == null) {
         System.out.println("Algorithm not choosen");
         algorithmNotChoosenLabel.setText("Please choose an algorithm");
      } else {
         if (itemListView.getItems().size() == 0) {
            algorithmNotChoosenLabel.setText("Please fill an items table");
         } else {
            algorithmNotChoosenLabel.setText("");
            if (selectAlgorithm.getSelectionModel().getSelectedItem().equals(AlgorithmNames.BRUTE_FORCE)) {
               knapsack = knapsackLib.bruteForceAlgorithm(new ArrayList<>(itemListView.getItems()),
                  new Knapsack(Integer.parseInt(capacity.getText())));
               System.out.println(knapsack);
            }
            if (selectAlgorithm.getSelectionModel().getSelectedItem().equals(AlgorithmNames.GREEDY)) {
               knapsack = knapsackLib.greedyAlgorithm(new ArrayList<>(itemListView.getItems()),
                  new Knapsack(Integer.parseInt(capacity.getText())));
               System.out.println(knapsack);
            }
            if (selectAlgorithm.getSelectionModel().getSelectedItem().equals(AlgorithmNames.RANDOM_SEARCH)) {
               RandomSearchConfig randomSearchConfig = new RandomSearchConfig(100);
               knapsack = knapsackLib.randomSearchAlgorithm(new ArrayList<>(itemListView.getItems()),
                  new Knapsack(Integer.parseInt(capacity.getText())), randomSearchConfig);
               System.out.println(knapsack);
            }
            assert knapsack != null;
            double worth = knapsack.calculateWorthOfItems();
            worth *= 1000;
            worth = Math.round(worth);
            worth /= 1000;
            worthOfItemLabel.setText(String.valueOf(worth));
            weightOfItemLabel.setText(String.valueOf(knapsack.calculateWeightOfItems()));
            resultListView.getItems().clear();
            resultListView.getItems().addAll(knapsack.getCopyOfItems());
         }
      }
   }

   @FXML
   private void onAdd() {
      int weightOfItem = Integer.parseInt(weightOfItemTextField.getText());
      double worthOfItem = Double.parseDouble(worthOfItemTextField.getText());
      Item item = new Item(worthOfItem, weightOfItem);
      ObservableList<Item> items = itemListView.getItems();
      items.add(item);
      itemListView.setItems(items);
   }

   @FXML
   private void onCreateRandom() {
      ObservableList<Item> randomList = itemListView.getItems();
      randomList.clear();
      Random random = new Random();
      int numberOfItems = 5 + random.nextInt(95);
      while (numberOfItems > 0) {
         numberOfItems--;
         Item item = new Item(random.nextDouble(), random.nextInt(20));
         randomList.add(item);
      }
      itemListView.setItems(randomList);
   }

   @FXML
   private void onAlgorithmChoose() {
      KnapsackLibraryApi knapsackLib = KnapsackLibrary.newInstance();
      Description description = null;
      if (selectAlgorithm.getSelectionModel().getSelectedItem().equals(AlgorithmNames.BRUTE_FORCE)) {
         description = knapsackLib.getBruteForceDescription();
      }
      if (selectAlgorithm.getSelectionModel().getSelectedItem().equals(AlgorithmNames.GREEDY)) {
         description = knapsackLib.getGreedyDescription();
      }
      if (selectAlgorithm.getSelectionModel().getSelectedItem().equals(AlgorithmNames.RANDOM_SEARCH)) {
         description = knapsackLib.getRandomSearchDescription();
      }
      assert description != null;
      algorithmDescriptionTextArea.setText(description.getInformation());
   }

   @Override
   public void initialize(URL location, ResourceBundle resources) {
      ObservableList<String> items = selectAlgorithm.getItems();
      items.add(AlgorithmNames.GREEDY);
      items.add(AlgorithmNames.RANDOM_SEARCH);
      items.add(AlgorithmNames.BRUTE_FORCE);
      selectAlgorithm.setItems(items);
      algorithmNotChoosenLabel.setTextFill(Color.web("#ff0000"));
      algorithmDescriptionTextArea.setWrapText(true);
      try {
         knapsackImage.setImage(new Image(new FileInputStream("resources/knapsack.png")));
      } catch (FileNotFoundException e) {
         e.printStackTrace();
      }
      setLanguage(LanguagesNames.POLISH);
   }

   private void setLanguage(String language) {
      if (language.equals(LanguagesNames.ENGLISH)) {
         english();
      }
      if (language.equals(LanguagesNames.POLISH)) {
         polish();
      }
   }

   @FXML
   private void onClose() {
      System.exit(0);
   }

   @FXML
   private void onPolish() {
      polish();
   }

   @FXML
   private void onEnglish() {
      english();
   }

   @FXML
   private void onAbout() {

   }

   private void polish() {
      itemPropertiesLabel.setText("Właściwości przedmiotu");
      weightLabel.setText("Waga:");
      worthLabel.setText("Wartość:");
      addButton.setText("Dodaj");
      selectAlgorithm.setPromptText("Wybierz algorytm");
      createRandomButton.setText("Utwórz losowo");
      numberOfItemsLabel.setText("Liczba przedmiotów:");
      resultLabel.setText("Lista wynikowa:");
      weightOfItems.setText("Waga plecaka:");
      worthOfItems.setText("Wartość plecaka:");
      startButton.setText("START");
      knapsackCapacityLabel.setText("Pojemność plecaka:");
      fileMenu.setText("Plik");
      closeMenuItem.setText("Zamknij");
      editMenu.setText("Edycja");
      languageMenu.setText("Język");
      polishMenuItem.setText("Polski");
      englishMenuItem.setText("Angielski");
      helpMenu.setText("Pomoc");
      aboutMenuItem.setText("O programie");
   }

   private void english() {
      itemPropertiesLabel.setText("Item properties");
      weightLabel.setText("Weight:");
      worthLabel.setText("Worth:");
      addButton.setText("Add");
      selectAlgorithm.setPromptText("Choose algorithm");
      createRandomButton.setText("Create random");
      numberOfItemsLabel.setText("Number of items:");
      resultLabel.setText("Result:");
      weightOfItems.setText("Weight of items:");
      worthOfItems.setText("Worth of items:");
      startButton.setText("START");
      knapsackCapacityLabel.setText("Knapsack Capacity:");
      fileMenu.setText("File");
      closeMenuItem.setText("Close");
      editMenu.setText("Edit");
      languageMenu.setText("Language");
      polishMenuItem.setText("Polish");
      englishMenuItem.setText("English");
      helpMenu.setText("Help");
      aboutMenuItem.setText("About");
   }
}
